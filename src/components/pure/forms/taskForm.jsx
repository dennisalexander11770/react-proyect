import React, { useRef } from 'react';
import ProtoType  from 'prop-types';
import { LEVELS } from '../../../models/levels.enum';
import { Task } from '../../../models/task.class';
const TaskForm = ({add,length}) => {
    const nameRef= useRef('');
    const descriptionRef= useRef('');
    const levelRef = useRef (LEVELS.NORMAL);

    function addTask(e) {
        e.preventDefault();
        const newTask = new Task(
            nameRef.current.value,
            descriptionRef.current.value,
            false,
            levelRef.current.value,
           
        )        
        add(newTask);
        
    }
    return (
        <form className='d-flex justity-content-center align-items-center mb-4' onSubmit={addTask}>
            <div className='form-outline flex-fill' >
                <input ref={nameRef} id='inputName' type='text' className='form-control form-control-lg' required  autoFocus placeholder='ingrese el nombre de la tarea'/>                
                <input ref={descriptionRef} id='inputdescription' type='text' className='form-control form-control-lg' required placeholder='ingrese la descripcion'  />                
                <labe htmlFor='selectLevel' className='sr-only'>
                    Priority
                </labe>
                <select ref={levelRef}  defaultValue={LEVELS.NORMAL} id='selectLevel'>
                    <option value={LEVELS.NORMAL}>Normal</option>
                    <option value={LEVELS.URGENT}>Urgent</option>
                    <option value={LEVELS.BLOCKING}>Blocking</option>
                </select>
            </div>
            <button type="submit"
                className="ms-2 btn btn-primary btn-lg"
                onClick={addTask}
            >
                Add
            </button>
            
        </form>
    );
}
TaskForm.ProtoType ={
    add: ProtoType.func.isRequired
}
export default TaskForm;
