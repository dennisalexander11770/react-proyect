import { useRef } from "react";
import React from 'react';

const Child = ({name , send ,update}) => {
    const nameRef=useRef('')
    const mensage = useRef('')
    function pressbutton() {
        const text = mensage.current.value
        alert(`text Input ${text}`)
    }
    function pressbuttonParam(text) {
        alert(`Text ${text}`)
        
    }
    function submitName(e) {
        e.preventDefault()
        update(nameRef.current.value);
    }
   
    return (
        <div>
            <p onMouseOver={
                () => {
                    console.log('on mause hover')
                }
            }>
                hello {name}
            </p>
            <button onClick={()=> console.log('button 1 pulsado ')}>
                Boton 1
            </button>
            <button onClick={
                pressbutton
            }>
                Boton 2
            </button>
            <button onClick={
                () =>pressbuttonParam('hellon new')

            }>
                Boton 2.3
            </button>
            <input 
            placeholder='este es un mensage para el padre'
            onFocus={console.log('focus no focus')}
            onChange={(e) =>console.log('input text integresion  ' , e.target.value)}
            onCopy={(e)=>console.log('se copio el testo del input')}
            ref={
                mensage
            }
            />
            <button onClick={ ()=>send(
                mensage.current.value
            )}>
                setMessegen
            </button>
            <div style={{marginTop:'20px' }}>
                <form onSubmit={submitName}>
                    <input ref={nameRef} placeholder="New Name"/>
                    <button type="submit">
                        Name update
                    </button>
                </form>

            </div>
        </div>
    );
}

export default Child;
