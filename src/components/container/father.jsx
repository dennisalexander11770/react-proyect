import React, { useState } from 'react';
import Child from '../pure/child';

const Father = () => {
    const [name, setName] = useState('martin');
    function updateName(newName ) {
            setName(
                newName
            )
    }
    function shoeMessage(text) {
        alert(`este una alerta: ${text} ` )
        
    }
    return (
        <div>
            <Child
             update={
                 updateName  
             }
             name={name} 
             send={
              shoeMessage  
            }>

            </Child>
        </div>
    );
}

export default Father;
