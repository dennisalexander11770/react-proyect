import logo from './logo.svg';
import './App.css';
import TaskListComponent from './components/container/task_list';
/*import Greeting from './components/pure/greeting';
import Greetingf from './components/pure/greetingF';

import Ejemplo1 from './hooks/Ejemplo1';
import Ejemplo2 from './hooks/Ejemplo2';
import MiComponenteConContexto from './hooks/Ejemplo3'
import Ejemplo4 from './hooks/Ejemplo4';*/
import Greetingstyled from './components/pure/greetingStyled';
import Father from './components/container/father';
import OpcionRender from './components/pure/opcionRender';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {/* Componente propio Greeting.jsx */}
        {/* <Greeting name={"Martín"}></Greeting> */}
        {/* Componente de ejemplo funcional */}
        {/* <Greetingf name="Martín"></Greetingf> */}
        {/* Componente de Listado de Tareas */}
        <TaskListComponent></TaskListComponent>
                {/* Ejemplos de uso de HOOKS */}
        {/* <Ejemplo1></Ejemplo1> */}
        {/* <Ejemplo2></Ejemplo2> */}
        {/* <MiComponenteConContexto></MiComponenteConContexto> */}
        {/* Todo loq ue hay aquí, es tratado como props.children */}
        {/* <Ejemplo4 nombre="Martín">
          <h3>
            Contenido del props.children
          </h3>
        </Ejemplo4> */}
       {/*<Greetingstyled name="Martín"></Greetingstyled>*/}        
        {/*<Father />*/}
        {/* ejecicon de renderisado condicional */}     
        {/*<OpcionRender />*/}
      </header>
    </div>
  );
}

export default App;
